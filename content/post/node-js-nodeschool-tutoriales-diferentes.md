---
title: 'Node.js, NodeSchool. Tutoriales diferentes'
id: 177
categories:
  - Programming
  - Tutorials
date: 2015-10-04 18:52:11
tags:
---

Llevo alrededor de 4 años haciendo tutoriales y por lo general siempre comparto con la comunidad y/o compañeros de trabajo cuando encuentro algo interesante, pero nunca me había planteado destinar un post del blog a esto,  después de unos días realizando estos tutoriales decidí que quizás este era el momento.

<!-- more -->

## ¿Qué les hace diferentes?

Esta serie de tutoriales están orientados como una 'aventura' o un juego por fases, tienes una serie de retos y el objetivo es ir superando uno a uno esos retos que entre ellos tiene mayor o menor relación pero que al final forman parte de un todo. El tutorial lo descargas y lo ejecutas en tu máquina como si de una aplicación se tratara, tiene su entorno de menú, con una lista de retos.

[![menu](/images/2015/10/menu.jpg)](/images/2015/10/menu.jpg)

Como podemos ver, los retos indican cuándo han sido completado y así tenemos la sensación de ir avanzando en una aventura. Cuándo seleccionamos un reto, nos imprime las instrucciones que además incluyen una serie de pistas

[![instrucciones](/images/2015/10/instrucciones.jpg)](/images/2015/10/instrucciones.jpg)

Aparte una vez escribimos nuestro pequeño script para superar la prueba tenemos a disposición unos test para verificar que el ejercicio es correcto

[![resultados](/images/2015/10/resultados.jpg)](/images/2015/10/resultados.jpg)

Para mi estos detalles marcan bastante la experiencia del usuario.

&nbsp;

## Muy bonitos... ¿Y son útiles?

Otro punto destacable es que el contenido de cada reto desde mi punto de vista está muy bien escogido y en cada paso se afianza un poquito más alguna característica de la tecnología o el lenguaje (los tutoriales son de node.js y conceptos de js).

En la 'aventura', así es como se llaman estos tutoriales, **_learnyounode,_**_ _vas avanzando desde lo más básico de node.js, hasta al final ser capaz de crear una mini API  que diferencia entre dos peticiones y genera la respuesta para ellas. Cada reto puede llevar de 15 a 20 minutos, quizás este es el comentario menos objetivo de toda mi valoración, puesto que el tiempo de cada ejercicio se verá afectado por nuestra experiencia, nuestra claridad en ese momento, etc. El mensaje que quiero trasmitir es que con ejercicios cortos, cómodos de entender e individuales pero con un objetivo común, pasamos de un "Hello World"  a un "HTTP API JSON SERVER"   realizando un camino que es apto para todos los niveles.

Es importante entender que con un sólo tutorial no vamos a ser expertos en una tecnología pero considero que para el tiempo empleado, los conocimientos "teórico/prácticos" obtenidos son bastantes gracias a la dinámica y formato que se nos presenta.

&nbsp;

## Open Source y Ampliable

&nbsp;

Como ya mencioné, estos tutoriales están construidos como aplicaciones que se ejecutan en tu máquina, pues bien el código de estas aplicaciones es libre y esta publicado en GitHub, para mí un punto a favor. Además han creado una increíble y genial comunidad en torno a la "escuela".

Dado mi poco nivel en JavaScript y Node.js, es poco probable que pueda aportar a un tutorial contenido teórico, sin embargo mis conocimientos técnicos me permiten aportar de otra manera. Por esto me he comprometido conmigo mismo para realizar algún cambio o mejorar alguno de estos tutoriales de aquí a final de este año.

Y quizás un paso más allá, dado que la 'escuela' ha creado un framework para desarrollar este tipo de 'aventuras'. Tengo ilusión por crear una aventura, me gustaría crear una pequeña aportación de aquí a final de año.

Espero que en poco tiempo, esté escribiendo un post-resumen de cómo he ayudado a mejorar una 'aventura' o incluso de cómo he creado una propia, un reto que me planteo y para el que sigo trabajando. Ahora mismo estoy realizando la aventura _**`scope-chains-closures.
`**_

&nbsp;

Recursos:

[NodeSchool](http://nodeschool.io)

[Mi solución Lear You Node (Aventura)](https://github.com/jdvr/learnyounode)

&nbsp;

&nbsp;