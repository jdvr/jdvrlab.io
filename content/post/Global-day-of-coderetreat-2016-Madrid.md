---
title: Global day of coderetreat 2016 @ Madrid
date: 2016-10-25 13:44:50
cover: /images/2016/10/gdcr-cover.jpg
cover_caption: Oficinas de idealista, Madrid
id: 206
categories:
  - work
  - craftsmanship
  - events
  - kata
tags:
---

This year as a member of [Software Craftsmanship Madrid](https://www.meetup.com/madswcraft/) I have had the pleasure to facilitate the [Global day of coderetreat](http://globalday.coderetreat.org/) in Madrid. We have been hosted by [idealista](https://www.idealista.com/) and the event has been amazing, a day to learn, share and improve as professional with 22 people for different companies and with other point of view.

We started early, at 9.15 I started a little introduction of 15 minutes, I spoke about Coderetreat, CoderDojo, idealista and Software Craftsmanship Madrid, then I explained the problem and the first constrain and we started the first session.d

## A Summary

I have shared a summary [here](https://github.com/SoftwareCraftsmanshipMadrid/global-day-of-coderetreat-2016/blob/master/summary.md) on the public community github.

<a class="twitter-moment" href="https://twitter.com/i/moments/807940125674139648">Global day of coderetreat 2016 @ Madrid</a>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

## What I learn

This was my third coderetreat, and every one is different you always learn something, this time I have the chance to share this coderetreat with a different community in a different city and as a facilitator. As a facilitator you have to be prepare to handle with everyone idea and try to share yours creating useful discussions, you have to choose very carefully every activity or restriction to help developers to move forward on the problem but keeping those who are slower on the track and not exclude anyone. I learn or confirm that a coderetreat really helps people to improve their skills, there were some people who haven't been on a coding dojo or kata before and they thanked us and confirmed he had learn a lot.

I am not able to sit here and write down every thing I had learn on this day. Everyone taught me something different, maybe something related with the way they code or maybe something related with the way the communicate their ideas, at the global the of coderetreat I think we improve every skill that help us as a professional developer, soft skills are also an important part of a good developer.

_Thanks to Software Craftsmanship Madrid members and thanks to idealista's employees who make this possible._
