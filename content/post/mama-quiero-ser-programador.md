---
title: 'Mama, quiero ser programador.'
id: 129
categories:
  - Programming
date: 2015-08-09 09:00:37
tags:
---

Durante el último año se ha producido varias veces una situación bastante normal pero que a mi me ha hecho reflexionar, normalmente en medio de una conversación, con alguien que acabas de conocer o un viejo amigo que hace tiempo que no ves, la otra persona suele comentar algo parecido a: "¿A qué te dedicas?" o "¿Qué eres?".
<!-- more -->
Por lo general la gente suele responder estas preguntas con la profesión asociada a su título universitario o estudios y a veces suelen destacar cuando lo estudiaron o donde. Yo normalmente respondo que soy "Desarrollador de Software", y los curiosos suelen añadir: "¿Estudiaste Ingeniería Informática o tienes un ciclo?". Creo que en nuestra profesión esa pregunta y su respuesta no tiene la misma validez que en otras.

Cuando un estudiante, o profesional, decide que quiere dedicarse a la programación, desde mi punto de vista el siguiente paso que debe tomar es coger las riendas de su formación y carrera e invertir su tiempo en formarse. En alguna entrevista me han preguntado: "¿Cuál es su formación?" a lo que yo suelo responder que mi formación reglada es la carrera pero la mayor parte de mi conocimiento y capacidades viene de actividades personales o con la comunidad fuera de lo que son mis estudios y que siempre realizo para intentar mejorar cada vez un poquito más.

Durante los últimos dos años, cuando era más consciente de la realidad de la profesión y mientras aún era estudiante universitario y programador, he visto muy buenos programadores atrapados en el estúpido circo del aprobado y el suspendido y he visto personas que han confiado toda su formación y carrera a los estudios universitarios.

Los primeros, una vez se dan cuenta de lo innecesario que es la formación reglada (porque ellos tienen conocimientos de sobra) no tienen problemas en encontrar un trabajo y disfrutar de nuestra profesión, por desgracia, los segundos,  tampoco suelen tener problemas, y una vez están en el mercado laboral echan la culpa de su falta de formación y habilidades a la universidad, pero claro, con el preciado título debajo del brazo.

En ningún momento quiero ofender a aquellas personas que se limitan a ir a clase y aprobar, sin mostrar interés o desarrollar las habilidades que un desarrollador necesita, pero creo que hacerlo es una buena forma de invertir el tiempo que pasábamos en la universidad.

Tampoco creo que la carrera sea una perdida completa de tiempo, puesto que los años que pasan en la universidad pueden ser el complemento perfecto a tu formación extra, los que me conocen saben que yo agradezco mi paso por la universidad porque me dio la oportunidad de conocer la profesión y de vivir hoy en día de un trabajo que me gusta y de la forma que quiero.

Para concluir, solo destacar que si alguna vez se plantean esta misma frase "quiero ser desarrollador de software", creo que deben tener en cuenta que en ese mismo momento se deben coger las riendas y poner voluntad y empeño en formarse y guiar paso a paso la tu carrera profesional hacia donde tú quieres. No hace falta llegar al extremo, no se trata de perder la vida social (como ya he oído), ni de unirse a una secta (estupidez que también he oído). Se trata de ser consecuentes y entender que solo tú eres responsable de apuntar tu carrera y tu vida en la buena dirección.