---
title: "Slimbook Katana: Mi opinión"
date: 2018-02-07 15:59:21
cover: /images/2018/02/katana-desktop.png
cover_caption: Mi escritorio
id: 208
categories:
  - Personal
  - Slimbook
  - Katana
  - Review
tags:
---

## Update (Noviembre de 2019)

Gracias a Slimbook por dejarme probar su nuevo ProX. El equipo es mucho mejor que el Katana y se nota el salto de calidad de la marca. Hasta que pueda escribir un nuevo articulo puedes leer mis opiones [aquí](https://twitter.com/juandvegarguez/status/1185507205275095048)


## Contexto

Hace unos días escribí unos tweets de lo que pensaba sobre mi Slimbook Katana. El problema de twitter es que es un medio muy estéril y además yo tampoco puse demasiado empeño así que me quedaron 3 tweets muy poco constructivos, llenos de punto negativos y sin contexto.

<blockquote class="twitter-tweet" data-conversation="none" data-lang="en"><p lang="es" dir="ltr">No lo volvería a comprar. No se cual seria la alternativa pero para mi no han cumplido lo que prometen. En estos tweets contesto a eso.<a href="https://t.co/ADmT0wTTpd">https://t.co/ADmT0wTTpd</a></p>&mdash; Juan David Vega (@juandvegarguez) <a href="https://twitter.com/juandvegarguez/status/958608533553115136?ref_src=twsrc%5Etfw">January 31, 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Aclaro también que durante estos 9 meses el Katana ha sido mi ordenador personal durante 7 u 8 meses y tenía otro para el trabajo. Desde principios de año pasó a ser mi portátil para uso personal y profesional.

## Como acabé teniendo un Slimbook

Seguramente esto parece lo menos importante para hacer una _review_ del portátil, pero de aquí sale mi tweet. Supe de Slimbook por twitter y buscando un poco encontré los programas con [Juan Febles de PodcastLinux](http://podcastlinux.com/). En los tres o cuatro programas que se habla de Slimbook se menciona la _"compatibilidad total"_ (no recuerdo si con esas palabras) del hardware que trae instalado con Linux. Es verdad que en su web esto no aparece (o no lo he encontrado), pero se pueden encontrar artículos que ellos enlazan donde se hace mención explícita a que este portátil esta pensado para GNU/Linux. Así que tras leer mucho.. **¡me lo compré!**.

Quizás mi problema fue esperar que realmente el hardware iba a tener **cero problemas**. Pero he encontrado los que tienen cualquier otro, por eso digo que _no cumplen lo que promenten_.

Como usuario de diferentes distros de GNU/Linux ya te esperas que algo no funcione bien... a menos que te aseguren lo contrario.

## Puntos fuertes

* Ligero
* Bonito (quizás lo menos importante pero tampoco está de más)
* Los componentes son buenos... realmente montan componentes de  calidad y eso se nota en el uso del portátil.
* Batería entorno a 4/5h programando y algo más de 6 navegando y escribiendo.
* Pantalla (es de lo que más me gusta)

## Que cambiaría yo...

* El micrófono está en un lugar _raro_ y muy cerca del ventilador lo que lo hace practicamente inútil.
* No se puede configurar el touchpad ([al menos en KDE Neon](https://bugs.kde.org/show_bug.cgi?id=383379))
* Avisar al usuario que pide una distro de errores conocidos. Aunque el ordenador se venda con linux está lejos del 100% de compatibilidad.
* Me costó disfrutar del portátil al principio. El ventilador hacía **mucho rudio**. Por suerte luego me ayudaron los del SAT


## Conclusión

No es un mal portátil, aunque tiene cosas que mejorar como todos. El servicio post-venta es excepcional y por esa parte seguramente Slimbook gana a otras marcas.

Aún dicho esto no lo volvería a comprar, seguramente habría optado por la otra alternativa. Pero no por la calidad del portátil o por el servicio de la marca, la realidad es que me gustaba más otro pero compré este exclusivamente por la supuesta compatibilidad. Que como comento no es muy diferenciadora del resto.

Espero que esto explique un poco mejor mi _decepción_. Si dentro de algunos años cuando tengan que comprar otro portátil Slimbook sigue en el mercado y ofrecen otro portatil bueno, seguramente esté entre mis opciones. Pero esta vez no me decantare por él basándose en la compatibilidad con GNU/Linux.

Gracias al equipo de Slimbook por escuchar a sus clientes.
