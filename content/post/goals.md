---
title: 'Passion, Learning and Pride.'
tags:
  - aboutme
  - intro
  - personal
id: 13
categories:
  - About me
  - aboutme
  - intro
  - personal
date: 2015-02-06 10:00:26
---

I think that these three words can define me, I am a young developer I know that my long journey to be a great programmer to archive mastery and become a Senior (experience) Developer, it is just starting now.
<!-- more -->
**Passion.** Some years ago when I started the university,  I did not know what programming was about, but when I wrote and ran my first "Hello World", that day I make programming my hobby, I was doing it for fun, today I enjoy every time I code, so programming for me it is not just a work.

**Learning.** I am a last year Software Engineer student at Las Palmas University, but it is just a little part of my education, I am self-taught, so I always dedicate a big part of my free time to learn about languages, technologies, methodologies, etc. During my daily life, I use to remember that thing that have been more difficult or the ones which made me get stuck and at afternoon at home I get one and try to understand what was the problem and which skills I have to improve to avoid it the next time.

**Pride.** This is the beautiful part of this profession, the final product, the working well-crafted software. Like Sandro Mancuso said, It is impossible no love this profession when you realize that you are creating something that help people on their daily life.