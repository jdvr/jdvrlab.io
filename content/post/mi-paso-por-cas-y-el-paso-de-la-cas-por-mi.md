---
title: Mi paso por CAS y el paso de la CAS por mi.
id: 231
categories:
  - About me
  - Eventos
  - Programming
date: 2015-12-06 09:00:50
tags:
---

Esta semana tuve la oportunidad de asistir a la Conferencia Agile Spain 2015 (CAS), en el Círculo de bellas artes de Madrid en el centro de la capital. Han sido dos días muy interesantes en los que como en otros eventos he ido con el interés y la ilusión de adquirir nuevos conocimientos e ideas, pero también con ganas de conocer y reencontrarme con muchos compañeros de profesión. En balance general mis expectativas se han cumplido, he tenido la oportunidad debatir ideas, expresar opiniones y asistir a charlas. A continuación voy a intentar resumir mis impresiones sobre los puntos de inflexión que han hecho que el evento merezca la pena.
<!-- more -->
# En el apartado general

&nbsp;

En dos día se han concentrado unos 700 profesionales del mundo del software, el trabajo de la organización ha sido increíble, si bien algunos empleados del propio recinto a veces tenían reacciones las cuales no comparto, los voluntarios siempre estaban dispuestos ayudar, comprometidos con su puesto y trabajando para hacer que la estancia de los asistentes en el evento fuera la mejor posible.

En cuanto al balance general de las Keynotes siento que alguna era muy interesante, pero sin embargo no tenían la esencia o el impacto de una keynote, aun así me gusto oír la forma de trabajo y la metodología de [unruly](https://unruly.co/) y la gran importancia de las retrospectivas que nombraba Corinna.

Hablando de la parte más importante del evento, las charlas, siento que a excepción de algunas (más adelante las nombré), el resto se quedaron un poco flojas, si bien es verdad que entiendo que la CAS no es solo un evento para la parte técnica del equipo, mi sensación fue que no se consiguió balancear el contenido técnico con el resto de tracks. Algo que no me resulto de buen gusto, fue que algunas charlas que sonaban como técnicamente buenas e importantes en el contexto que se desarrollaba el evento, fueran asignadas a salas pequeñas, mientras que otras que apenas generaron interés tenían salas de casi el doble de tamaño.

# La Keynote

&nbsp;

Me gustaría dedicar unos párrafos para lo que para mí fue la verdadera keynote del evento, bastante lejos del resto y causando un impacto y haciendo que muchos de nosotros reflexionáramos, [Leo Antoli](https://twitter.com/lantoli), consiguió para mi gusto un buen mensaje, contundente y claro. Por desgracia no tuve la oportunidad de debatir con él en personas algunas de las opiniones que tenía sobre su charla, pero seguro que tendré esa oportunidad.

Por nombrar la parte que menos me gusto primero, diré que la charla de Leo Antoli podría pecar de hacer que alguno hiciera justo lo contrario a lo que el predica y justificarse en su charla para demostrar que las katas, las buenas practicas, y la preocupación por el producto en general no son importantes. Aunque tampoco afirmo que tenga una efectividad real. Solo digo que a mí, y a mis compañeros más cercanos hasta el momento nos han sido útiles.

Llegados a este punto solo puedo decir que hay un mensaje claro que todos debemos tener en mente y aplicar en cada fase y momento de nuestro trabajo diario. **_Cuestiona tus prácticas, pregunte un porque, confirma que su efecto en ti es positivo y no asegures que son mejores que las de los otros si no lo puedes demostrar. La realidad es que solo eres capaz de afirmar, que parar ti funciona._**

# Lo que hubiera sido una pena que te perdieras

&nbsp;

Si no fuiste a la CAS o si fuiste pero no pudiste verlo todo y ahora te planteas _¿Qué me habré perdido? _Voy a empezar un brevisimo y subjetivo resumen_. _En este momento quiero destacar que toda mi opinión se basa en el track **“Mejorando Software”.**

La primera charla que podría marcar una diferencia significativa en como los asistentes de la CAS desarrollan software es **_“Escapando del modelo de desarrollo mete-saca”. _**En esta charla [Ricardo Borillo](https://twitter.com/borillo) aparte de mencionar y cuestionar el por qué seguimos construyendo CRUD como si fueran aplicaciones de última generación, nos hacía cuestionarnos el por qué si nuestra API REST es sólo consumida por nuestro propio sistema, por qué tiene tantos endpoints que hacen que las aplicaciones crezcan y sean muy complicadas de mantener. La realidad es que crear un único endpoint que funcione como una interfaz de comandos sería más efectivo y escalable. En resumen, una muy buena charla que ya me ha puesto manos a la obra para preparar y luego intentar facilitar una kata en la que trabajemos estos temas.

La siguiente charla a destacar es **_“Economía del Software y dependencias: dilemas del software desacoplado” _**críticas personales a parte, es interesante justificar nuestras prácticas desde el punto de vista económico y de negocio. Estas charlas siempre me recuerdan la idea de que debemos estar siempre atentos que nuestro software debe ser adaptable y escalable cuando se producen cambios pero que, sobre todo nuestro software, es un producto que debe ser rentable y que todas nuestras acciones y decisiones tienen un impacto en el beneficio que ese software proporciona al cliente.

No me quiero extender más, así que me voy a limitar a dar unas recomendaciones. Si vas a ver los videos y no sabes que charlas ver, puedes empezar por: **_“El arte de dar Feedback (Comunicación Verbal No Violenta)”, “Building resilient integrations”, “Keynote: Leo Antoli”, “Dando amor a los tests” y “Effective UI Testing”_**. Algunas me gustan más y otras menos, pero para no detenerme en cada una, creo que todas pueden aportar algo y merece la pena verlas.

# Conclusiones

Aunque he disfrutado bastante el evento y me ha encantado tanto las charlas como el networking, para mí como experiencia personal queda claro que el tipo de evento es bueno y debe seguir adelante, pero siempre centrándose en mantener la calidad de las chalas y tener todos los tracks balanceados. ¿Quizás deberíamos tener un evento puramente técnico y otro orientado a cómo funciona los equipos y organizaciones? Hablando de otro tema, en mi caso particular quiero recalcar lo importante y positivo que considero que las empresas apuesten por enviar a sus empleados a este tipo de eventos. No pasará demasiado tiempo hasta que la empresa vea que su inversión se transforma en mejora de la calidad del resultado del trabajo diario de los empleados, porque no puedo demostrar ni afirmar que el resultado es casi inmediato.

[![IMG_20151206_214322](/images/2015/12/IMG_20151206_214322.jpg)](/images/2015/12/IMG_20151206_214322.jpg)