---
title: Hi, I'm Juan
description: "This page is under construction... "
layout: "about"
aliases:
    - /about-me/
    - /cv/
---

I am a Software Engineer from Spain. I like to work with amazing teams using XP practices such as Pair Programming, TDD, CI, CD to build great products. I'm also fond of DevOps Culture and I always try to code in different layers (frontend, backend, IaC...).

I learned a lot from the communities in my first year as programmer so, I think I have to give all the knowledge back. I am an active member of [Software Crafter Madrid](https://www.meetup.com/madswcraft/), and this year, for first time, I have the opportunity to work with the [Global Day of Code Retreat](https://www.coderetreat.org/) organizers to make this great event possible one more year. I also love Free Open Source Software and although I am not a major contributor, [I always try to put my grain of sand](#foss).

I studied Software Engineer at University but that is only a little part of all my education, I am self-taught and I have learned languages, technologies and practices on my own. I love learning and discovering about everything.

Nowadays as a programmer you can work wherever you want as long as you have and internet connection. I think remote work is a must to keep a good life-work balance and despite I live in Madrid I would like to be in a team with people from everywhere in the world.

## Have you been looking for me?...

I like to build software, and I love to see the impact of that software. I have worked in small start-up starting everything from the beginning and I have worked with a code base with hundred of thousand line of code without a single test.

When building software I see the code as a means, If you are able to find a proper solution to the problem without a single line of code. Probably that solution is cheaper to implement and cheaper to maintain than code.

I you have landed here looking for a programmer and you think I am a good candidate, dont worry about the stack you work with. I wont be afraid of that, drop me a line and let's try to build something together.

_"The good news is there are no right or wrong ways when we have to quest to learn. It’s all about learning and relearning"_ :)

## Experience
---
### Software Engineer - [Buyviu](https://buyviu.com)
<span class="dates" >Jan 2018 - Present</span>

They hired three persons including me, to start the whole project. We started with a MVP that was a simple listing page that includes fashion products which we download from an affiliate network. We have worked with the business to evolved the product. 6 months later we have a completely iterative work methodology that has made us archived a full working product that changes everyday.

To reach our goal we have been using Extreme Programming practices and values, DevOps culture and Kanban to keep the business and the tech team together pushing in the same direction.

- Developer (Python, JavaScript)
- Infrastructure as code (Ansible, Packer)
- Extreme programming
- DevOps Culture
- TBD
- Pair Programmig
- CI/CD
- At least 3 out of 5 days working remote

### Infrastructure Engineer - [Idealista SA](https://idealista.com)
<span class="dates" >Jun 2017 - Jan 2018</span>

After two years working at idealista, they offered me a challenge: make easier for the rest of the team develop and operates services.

We develop some internal tools and we change the way we were using tools like Jenkins, but the best part was work with the other 60 people in the idealista tech team to improve our DevOps culture.

- Infrastructure as code (Ansible)
- Test Driven Infrastructure (Ansible, Molecule, Goss)
- CI
- At least 2 or 3 out of 5 days working remote
- DevOps culture
- Promote internal training (Java, Ansible, TDD...)
- Environment Architecture/Platform development
- In a nutshell make other developer live easier...

### Software Engineer - [Idealista SA](https://idealista.com)
<span class="dates" >Ago 2015 - Jun 2017</span>

During almost two years I have been part of a core business development team, working every day to improve the internal process that allows us to sell our product. Working shoulder to shoulder with business people to improve process and create useful automation that make easier our relation with customer. I have improved my soft skills as well as my technical understanding of a continuous digital transformation.

On the other hand at the beginning we had hundred of thousand line of code without any test and with a huge lack of quality. We started working as a team, introducing refactoring techniques and test. We always tried to the get the minimum code to validate our solution. After a year we had extracted the most critical parts to its own well tested and designed module that could evolved as fast as the business needs.

- Test Driven Development
- Pair programming
- 30 to 40 days in a row per year working remote

### Software Engineer - [Yapiko Software House](https://www.yapiko.com/)
<span class="dates" >Jul 2015 - Ago 2015</span>

I joined Yapilko as Java developer to help with a sports bet project, but they had some problems. During my month in Yapiko I spend the time creating automation for dev environment.

- E-Commerce with WordPress.
- Dev Working Environment automation (vagrant, shell and puppet)


### Software Engineer - [Singular Factory](https://www.singularfactory.com/)
<span class="dates" >Jul 2014 - Jul 2015</span>

I arrived Singular one year before I finished degree while I was studying I was looking for a place where I was able to put into practices all the stuff I had been learning by myself.

During one year I worked with different teams from Android to Web including also some weeks in game development with Unity.

- Android
- Unity
- Symfony2
- TDD

## Education

- Software Engineer Degree<br/>
  2011 - 2015<br/>
  Universidad de Las Palmas

## Volunteer

During the last 5 years I have been an active member of different software development communities: _"Gran Canaria Ágil", "Software Crafter Gran Canaria", and "Software Crafter Madrid"_. During this time I have organized talks, code retreats and katas.

### FOSS

I haven't contributed to major open source project but I have a few pull request on [github](https://github.com/jdvr). I love and believe in open source software, and we I have had the opportunity to free code, [I have done](https://github.com/idealista). The experience of starting to open source code as idealista employer was amazing. I also use github to share all path throw different language and technology leaning path.

At University I spent a lot of time with student groups learning and teaching programming, we organize programming challenges and [we helped one of our professor](https://github.com/jcrodriguez-dis/moodle-mod_vpl/blob/c444fde6a4335f099969670dffa41a2c52c18775/AUTHORS.txt#L14) as a first real world task.
