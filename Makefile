# The following are targers that do not exist in the filesystem as real files and should be always executed by make
.PHONY: build start

build:
	rm -rf public
	docker run --rm -it -v $(PWD):/src -p 1313:1313 -u hugo jguyomard/hugo-builder hugo

# Run the development environment in the background
start:
	rm -rf public
	docker run --rm -it -v $(PWD):/src -p 1313:1313 -u hugo jguyomard/hugo-builder hugo server -w --bind=0.0.0.0
